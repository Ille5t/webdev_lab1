import React from 'react';
import Image from "../../components/UI/Images/Image";
const Main = () => {

    const [anim, setAnim] = React.useState(false);
    const [anim2, setAnim2] = React.useState(false);

    return (
        <main className={`${anim2 ? "anim" : ""}`}>
            {!anim ? <svg className={"touch"} width="462" height="462" viewBox="0 0 462 462" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                    <path d="M361.815 219.456C361.623 219.451 361.43 219.447 361.238 219.445C352.776 219.523 344.625 222.647 338.278 228.245C333.645 212.945 319.701 202.357 303.718 202.005C295.767 202.125 288.093 204.947 281.958 210.005C278.811 192.733 264.063 179.983 246.518 179.365C239.028 179.382 231.74 181.792 225.718 186.245V166.885C269.275 145.324 287.105 92.5354 265.544 48.9794C243.983 5.42237 191.194 -12.4076 147.638 9.15337C104.081 30.7144 86.2513 83.5034 107.812 127.059C116.366 144.339 130.358 158.331 147.638 166.885V251.445L129.398 226.885C116.762 210.199 93.0123 206.872 76.2783 219.445C60.6373 232.837 58.5083 256.251 71.4783 272.245L141.958 410.805C164.118 454.405 199.478 460.485 213.958 461.045C226.438 461.605 239.238 461.765 251.478 461.765C290.038 461.765 323.478 459.765 323.478 459.765H324.278C382.118 459.765 398.278 397.605 398.758 364.485V258.485C399.334 237.506 382.794 220.032 361.815 219.456ZM147.318 109.833C147.318 109.837 147.318 109.841 147.318 109.844V148.084C126.908 134.836 114.57 112.176 114.518 87.8444C114.32 48.0804 146.394 15.6844 186.158 15.4864C225.922 15.2884 258.318 47.3624 258.516 87.1264C258.639 111.775 246.144 134.774 225.398 148.085V109.845C225.401 88.2844 207.925 70.8024 186.364 70.7994C164.802 70.7964 147.321 88.2724 147.318 109.833ZM322.198 443.765C321.638 443.765 265.158 447.285 214.198 445.045C202.838 444.565 174.198 439.525 156.118 403.525L85.3983 264.245C85.1513 263.763 84.8573 263.307 84.5183 262.885C76.5563 253.771 77.4503 239.938 86.5183 231.925C96.2393 224.929 109.776 227.031 116.918 236.645L148.918 280.725L149.398 281.125C150.102 281.877 150.91 282.524 151.798 283.045L153.158 283.525H154.758H155.318H156.198H157.798L159.238 282.885H160.038L160.438 282.485C160.856 282.129 161.232 281.726 161.558 281.285C161.863 280.912 162.131 280.51 162.358 280.085C162.559 279.647 162.719 279.192 162.838 278.725C162.891 278.193 162.891 277.657 162.838 277.125C162.838 277.125 162.838 277.125 162.838 276.565V109.845C162.838 97.1204 173.153 86.8054 185.878 86.8054C198.603 86.8054 208.918 97.1204 208.918 109.845V251.445C208.918 255.863 212.5 259.445 216.918 259.445C221.336 259.445 224.918 255.863 224.918 251.445V218.405C223.683 206.94 231.977 196.644 243.443 195.41C254.908 194.175 265.204 202.469 266.438 213.935C266.598 215.421 266.598 216.92 266.438 218.406V275.926C266.438 280.344 270.02 283.926 274.438 283.926C278.856 283.926 282.438 280.344 282.438 275.926V241.046C281.203 229.581 289.497 219.285 300.963 218.051C312.428 216.816 322.724 225.11 323.958 236.576C324.118 238.062 324.118 239.561 323.958 241.047V305.047C323.958 309.465 327.54 313.047 331.958 313.047C336.376 313.047 339.958 309.465 339.958 305.047V258.487C338.723 247.022 347.017 236.726 358.483 235.492C369.948 234.257 380.244 242.551 381.478 254.017C381.638 255.503 381.638 257.002 381.478 258.488L381.958 364.328C381.958 367.605 380.038 444.325 322.198 443.765Z" fill="white"/>
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect width="461.766" height="461.766" fill="white"/>
                    </clipPath>
                </defs>
            </svg> : null}
            <div className={`startOverlay ${anim ? "anim" :""}`} onMouseEnter={()=>{if (!anim) setAnim(true)}}>
                <span>A long time ago in a galaxy far,<br/> far away....</span>
            </div>
            <div className={`title ${anim ? "anim" :""}`} onAnimationStart={()=>{const audio = new Audio(`${process.env.PUBLIC_URL}/assets/ost.mp3`); audio.play(); console.log(`${process.env.PUBLIC_URL}/assets/ost.mp3`)}}>
                <Image src={"star-wars-4.svg"}/>
            </div>
            <p className={`text ${anim ? "anim" :""}`} onAnimationEnd={()=>{setAnim2(true)}}>
                <span>Episode V</span><br/>
                <span>THE EMPIRE STRIKES BACK</span> <br/>
                It is a dark time for the Rebellion. Although the Death Star has been destroyed, Imperial troops have driven the Rebel forces from their hidden base and pursued them across the galaxy.
                Evading the dreaded Imperial Starfleet, a group of freedom fighters led by Luke Skywalker have established a new secret base on the remote ice world of Hoth.
                The evil lord Darth Vader, obsessed with finding young Skywalker, has dispatched thousands of remote probes into the far reaches of space....
            </p>

            <Image src={"planet.png"} className={`planet first ${anim2 ? "anim" : ""}`}/>
            <Image src={"planet2.png"} className={`planet second ${anim2 ? "anim" : ""}`}/>
            <Image src={"ship.png"} className={`ship ${anim2 ? "anim" : ""}`}/>
        </main>
    );

};

Main.propTypes = {};

export default Main;
